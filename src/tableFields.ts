module.exports = [
    { key: 'companyName', label: 'Company' },
    { key: 'ticker', label: 'Ticker' },
    { key: 'lastPrice', label: 'Last' },
    { key: 'priceChange', label: '%' },
    { key: 'bestBid', label: 'Bid' },
    { key: 'bestAsk', label: 'Ask' },
    { key: 'trades', label: 'Trades' },
    { key: 'volume', label: 'Volume' },
    { key: 'turnover', label: 'Turnover' }
]
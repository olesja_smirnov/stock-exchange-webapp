module.exports = {
  configureWebpack: config => {
    const versionTag = (new Date()).getTime();
    config.output.filename = `js/[name].${versionTag}.js`;
    config.output.chunkFilename = `js/[name].${versionTag}.js`;
  }
};

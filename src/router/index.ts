import Vue from 'vue'
import VueRouter from 'vue-router'

import DailyView from '../views/DailyView.vue'
import HomePage from '../views/HomePage.vue'
import TopTradedStocks from '../views/TopTradedStocks.vue'


Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'HomePage',
    component: HomePage,
    meta: {
      anon: true
    }
  },
  {
    path: '/stock/:date',
    name: 'DailyView',
    component: DailyView,
    meta: {
      anon: true
    }
  },
  {
    path: '/top',
    name: 'TopTradedStocks',
    component: TopTradedStocks,
    meta: {
      anon: true
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
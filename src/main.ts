import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { BootstrapVue, IconsPlugin, TablePlugin, ToastPlugin } from 'bootstrap-vue'
import axios, { AxiosStatic} from "axios";

import './scss/custom.scss'

Vue.config.productionTip = false;

declare module 'vue/types/vue' {
  interface Vue {
    axios: AxiosStatic;
  }
}

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(TablePlugin);
Vue.use(ToastPlugin);
Vue.use({
  install() {
    Vue.prototype.axios = axios.create({
      baseURL: process.env.VUE_APP_INTERNAL_API,
    });
  }
});

new Vue({
  router,
  created: function () {

    this.axios.interceptors.response.use(
        function (response) {
          return response;
        },

        function (error) {
          if (error.response) {
            let message: string;
            if (typeof error.response.data == "string") {
              message = error.response.data;
            } else {
              message = error.response.data.message || error.response.data.error;
            }
            if (!message || !message.length) {
              message = error.response.statusText.toUpperCase();
            }
            const vm = new Vue();
            vm.$bvToast.toast("ERRORS." + message, {
              title: "ERROR".toString(),
              variant: 'danger',
              autoHideDelay: 5000,
            })
          }
        });
  },
  render: h => h(App)
}).$mount('#app');